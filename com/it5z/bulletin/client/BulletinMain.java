package com.it5z.bulletin.client;

import net.minecraftforge.common.MinecraftForge;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;

@Mod(modid = "bulletin", name = "Bulletin Mod", version = "0.0.1 Pre-Beta")
@SideOnly(Side.CLIENT)
public class BulletinMain {
	@Instance("bulletin")
	public static BulletinMain instance;
	
	@Mod.EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(OpenMenuHandler.instance);
	}
}
