package com.it5z.bulletin.client;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.client.gui.GuiMainMenu;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.event.ForgeSubscribe;

@SideOnly(Side.CLIENT)
public class OpenMenuHandler {
	public static OpenMenuHandler instance = new OpenMenuHandler();
	
	@ForgeSubscribe
	public void openMenu(GuiOpenEvent event) {
		if(event.gui instanceof GuiMainMenu) {
			event.gui = new BulletinGuiMainMenu();
		}
	}
}
